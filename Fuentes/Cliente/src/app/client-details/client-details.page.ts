import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AlertController } from '@ionic/angular';

import { CartService } from './../services/cart.service';


@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.page.html',
  styleUrls: ['./client-details.page.scss'],
})
export class ClientDetailsPage implements OnInit {

  data: any;

  constructor(private route: ActivatedRoute, private router: Router, private cartService: CartService, public alertController: AlertController) {
 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.data = JSON.parse(params.special);
      }
    });

  }

  ngOnInit() {
  }

  async confirmAddClientToCart(client) {

    const alert = await this.alertController.create({
      header: 'Asociar cliente al carrito.',
      subHeader: '¿Está seguro de que desea asociar el cliente ' + client.name + ' al carrito?',
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.addClientToCart(client);
            this.router.navigateByUrl('/tabs/cart'); 
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['Cancel', 'Open Modal', 'Delete']
    });

    await alert.present();
  }

  addClientToCart(client) {
    this.cartService.addClientToCart(client);
  }

  

}
