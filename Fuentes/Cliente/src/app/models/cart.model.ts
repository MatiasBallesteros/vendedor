import { Product } from './product.model';
import { Client } from './client.model';

export interface Cart{
    client: Client;
    products: CartQuantity;
}

export interface CartQuantity{
    product: Product;
    quantity: number;
}