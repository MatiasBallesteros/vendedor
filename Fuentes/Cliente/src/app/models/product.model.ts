export interface Product{
    id: number;
    name: String;
    description: String;
    brand: String;
    image: String;
    price: number;
}