export interface Client{
    id: number;
    dni: number;
    name: String;
    surname: String;
    cell_phone: number;
    email: String;
    position: Position;
    distance: number;
}

interface Position {
    latitude: number;
    longitude: number;
}
