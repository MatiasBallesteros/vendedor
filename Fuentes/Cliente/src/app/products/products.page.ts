import { Component, ViewChild } from '@angular/core';
import { Product } from '../models/product.model';
import { ProductService } from '../services/product.service';
import { LoadingService } from '../services/loading.service';
import { Router, NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage{

  products: Product[] = [];
  someProducts = [];
  searchText = '';
  viewskeleton = false;
  constructor(private productService: ProductService, private router: Router, public loading: LoadingService) {

  }

  ngOnInit() {
    
    if (this.products.length == 0) {
      this.productService.getProducts().subscribe( 
        resp => {
          this.products = resp;
          this.viewskeleton = true;
        }
      );  
    }
    
  }

  showProductDetails(product) {
    
    let productDetails: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(product)
      }
    };
    this.router.navigate(['product-details'], productDetails);
  }

  SearchProduct( event ){ 
    const text = event.target.value;
    this.searchText = text;
  }
}
