import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Product } from './../models/product.model';
import { CartQuantity } from '../models/cart.model';

import { CartService } from './../services/cart.service';
import { Client } from '../models/client.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: 'cart.page.html',
  styleUrls: ['cart.page.scss']
})
export class CartPage implements OnInit {

  // private cart: Array<Product> = [];
  private cartQuantity: Array<CartQuantity> = [];
  private subscription: Subscription;
  private totalPrice: number;
  private cartClient: Client;

  constructor(private cartService: CartService, private router: Router) { }

  ngOnInit() {

    this.cartService.getCart().subscribe(data => {

      this.cartQuantity = data;
    },
      error => alert(error)
    );

    this.cartService.getCartPrice().subscribe(data => {

      this.totalPrice = data;
    },
      error => alert(error)
    );
    
    this.cartService.getCartClient().subscribe(data => {

      this.cartClient = data;
    },
      error => alert(error)
    );


  }

  addQuantity(id) {
    this.cartService.addQuantity(id);
  }

  subQuantity(id) {
    this.cartService.subQuantity(id);
  }

  removeFromCart(id){
    this.cartService.removeFromCart(id);
  }

  showClientsTab(){
    this.router.navigateByUrl('/tabs/client'); 
  }

}
