
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
// import { ReplaySubject } from 'rxjs';
import { Observable } from 'rxjs';
// import { delay } from 'rxjs/operators';

import { CartQuantity } from '../models/cart.model';
import { Product } from './../models/product.model';
import { Client } from '../models/client.model';


@Injectable({
    providedIn: 'root'
  })
  
export class CartService {

    private subject: BehaviorSubject<CartQuantity[]> = new BehaviorSubject([]);
    private subjectTotalPrice: BehaviorSubject<number> = new BehaviorSubject(0);
    private subjectClient: BehaviorSubject<Client> = new BehaviorSubject(null);
    private cartQuantity: CartQuantity[] = [];
    private totalPrice: number = 0;
    private uniquePrice: number = 10;

  constructor() { 
    this.subject.subscribe(data => this.cartQuantity = data);
    this.subjectTotalPrice.subscribe(data => this.totalPrice = data);
  }

  addProductToCart(product: Product) {
    let response = this.cartQuantity.findIndex(e => e.product.id === product.id);
    if (response == -1) {

      let newItem = {
        client: null,
        product: product,
        quantity: 1
      }
        
      this.subject.next([...this.cartQuantity, newItem]);

      this.totalPrice += this.uniquePrice;
      this.subjectTotalPrice.next(this.totalPrice);

        return true;
    }else{
        return false;
    }
  }
  
  addClientToCart(client: Client) {
    this.subjectClient.next(client);
    console.log(this.subjectClient);
  }
  
  removeFromCart(id) {

    let response = this.cartQuantity.findIndex(e => e.product.id === id);   
    this.totalPrice -= this.cartQuantity[response].quantity * this.uniquePrice;
    this.subjectTotalPrice.next(this.totalPrice);

    let newCartProducts = this.cartQuantity.filter(e => e.product.id !== id);
    this.subject.next(newCartProducts);
  }

  getCart(): Observable<CartQuantity[]> {
    return this.subject;
  }
  
  getCartQuantity(): CartQuantity[] {
    return this.cartQuantity;
  } 

  getCartPrice(): Observable<number>{
    return this.subjectTotalPrice;
  }
  
  getCartClient(): Observable<Client>{
    return this.subjectClient;
  }

  clearCart(){
    this.subject.next(null);
  }

  addQuantity(id){
    let response = this.cartQuantity.findIndex(e => e.product.id === id);    
    this.cartQuantity[response].quantity += 1;

    this.totalPrice += this.uniquePrice;
    this.subjectTotalPrice.next(this.totalPrice);

  }

  subQuantity(id){
    let response = this.cartQuantity.findIndex(e => e.product.id === id); 
    
    if (this.cartQuantity[response].quantity >= 2) {
      this.cartQuantity[response].quantity -= 1; 

      this.totalPrice -= this.uniquePrice;
      this.subjectTotalPrice.next(this.totalPrice);
    }
    
  }
  
}