import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from '../models/client.model';

import { delay } from 'rxjs/operators';
import { apiURL } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ClientService {

  constructor( private http: HttpClient) { }

  getClients() {

    return  this.http.get<Client[]>( apiURL.clientsListURL )
              .pipe(
                delay( 2500 )
              );

  }
}