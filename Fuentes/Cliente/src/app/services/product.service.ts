import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product.model';

import { delay } from 'rxjs/operators';
import { apiURL } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class ProductService {

  constructor( private http: HttpClient ) { }

  getProducts() {

    return  this.http.get<Product[]>( apiURL.productsListURL )
              .pipe(
                delay( 2500 )
              );

  }
}