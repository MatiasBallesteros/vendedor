import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { CartService } from './../services/cart.service';



@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {

  data: any;

  constructor(private route: ActivatedRoute, private router: Router, private cartService: CartService, public alertController: AlertController) {

    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.data = JSON.parse(params.special);
      }
    });

  }

  ngOnInit() {
  }

  async addProductToCart(product) {
    let response = await this.cartService.addProductToCart(product);
    console.log(response);

    if (response) {
      this.addedProduct();
    } else {
      this.errorAddingProduct();
    }
  }

  async addedProduct() {
    const alert = await this.alertController.create({
      header: 'Producto agregado correctamente.',
      buttons: [
        {
          text: 'Cerrar'
        },
        {
          text: 'Ver carrito',
          handler: () => {
            this.router.navigateByUrl('/tabs/cart'); 
          }
        }
      ]
    });

    await alert.present();
  }

  async errorAddingProduct() {
    const alert = await this.alertController.create({
      header: 'El producto ya se encuentra en el carrito.',
      buttons: [
        {
          text: 'Cerrar'
        },
        {
          text: 'Ver carrito',
          handler: () => {
            this.router.navigateByUrl('/tabs/cart'); 
          }
        }
      ]
    });

    await alert.present();
  }




}
