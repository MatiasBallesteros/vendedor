import { Component } from '@angular/core';
import { Client } from '../models/client.model';
import { Location } from '../models/location.model';
import { ClientService } from '../services/client.service';
import { Router, NavigationExtras } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { AuthService } from '../services/auth.service';

import { Geolocation } from '@ionic-native/geolocation/ngx';
// import { Location } from '@angular/common';



@Component({
  selector: 'app-client',
  templateUrl: 'client.page.html',
  styleUrls: ['client.page.scss']
})
export class ClientPage {

  clients: Client[] = [];
  searchText = '';
  viewskeleton = false;
  mobileGeolocation: Location = { latitude: 0, longitude: 0 };

  constructor(private authService: AuthService, private clientService: ClientService, private router: Router, public loading: LoadingService, private geolocation: Geolocation) {
    
  }

  ngOnInit() {
    this.initializeClients();
  }

  async initializeClients() {
    if (this.clients.length == 0) {
      await this.clientService.getClients().subscribe(
        resp => {
          this.clients = resp;
          this.viewskeleton = true;

          this.updateDistanceToClients();

        }
      );
    }

  }

  async updateDistanceToClients() {
    await this.updateGeolocation();

  }

  async updateGeolocation() {

    await this.geolocation.getCurrentPosition().then((resp) => {
      this.mobileGeolocation.latitude = resp.coords.latitude;
      this.mobileGeolocation.longitude = resp.coords.longitude;
      
      this.clients.forEach( (element, index) => {
        let distance = this.getDistanceFromLatLonInKm(this.mobileGeolocation.latitude, this.mobileGeolocation.longitude, element.position.latitude , element.position.longitude);
        this.clients[index].distance = this.truncKilometers(distance);
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  truncKilometers(n) {
    let t=n.toString();
    let regex=/(\d*.\d{0,2})/;
    return t.match(regex)[0];
  }


  logout(){
    this.authService.doLogout()
    .then(res => {
      this.router.navigate(["/login"]);
    }, err => {
      console.log(err);
    })
  }


  showDetails(client) {

    let clientDetails: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(client)
      }
    };
    this.router.navigate(['client-details'], clientDetails);
  }

  SearchClient(event) {
    const text = event.target.value;
    this.searchText = text;
  }

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }

}