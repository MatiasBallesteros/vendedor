import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: any[],
            text:string,
            column:string  
            ): any[] {
    if (text.length === 0 ) {return array;}
      
    text = text.toLocaleLowerCase();
    
    return array.filter(item => {
      return item[column].toLocaleLowerCase().includes(text)})

    }

}
