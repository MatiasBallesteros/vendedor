﻿using System;
using System.Linq;
using System.Reflection;
using Servidor.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using System.IO;
using Servidor.Services.Interfaces;
using Servidor.Services;
using FirebaseAdmin;
using Firebase.Auth;
using System.Diagnostics;
using Helpers.Exceptions;
using Google.Apis.Auth.OAuth2;

namespace Servidor
{
    public class Startup
    {
        private const string TOKEN_HEADER_NAME = "x-id-token";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCors();

            //Crea una instancia de FirebaseApp. Es lo que el inyector de dependecia resuelve.
            services.AddSingleton((provider) =>
            {
                var firebaseApp = FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.FromFile("privatekey.json"),
                });

                return firebaseApp;
            });

            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IProductService, ProductService>();

            services.AddSingleton<ISessionFactory>((provider) =>
            {
                var cfg = new NHibernate.Cfg.Configuration();
             
               string connectionString = Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.Configuration, "DefaultConnection");
                
               cfg.DataBaseIntegration(config =>
                {
                    config.ConnectionProvider<DriverConnectionProvider>();
                    config.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                    config.Timeout = 60;
                    config.LogFormattedSql = true;
                    config.LogSqlInConsole = false;
                    config.AutoCommentSql = false;
                    config.Dialect<MsSql2012Dialect>();
                    config.Driver<Sql2008ClientDriver>();
                    config.ConnectionString = connectionString;
                    config.SchemaAction = SchemaAutoAction.Recreate;
                }); 

                ConventionModelMapper conventionalMappings = CreateConventionalMappings();
                var mapping = conventionalMappings.CompileMappingFor(GetType().Assembly.GetExportedTypes());
                cfg.AddDeserializedMapping(mapping, "server");

                var sessionFactory = cfg.BuildSessionFactory();
#if DEBUG
                try
                {
                    using (var session = sessionFactory.OpenSession())
                    using (var transaction = session.BeginTransaction())
                    using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Servidor.Scripts.initialData.sql"))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        var lines = reader.ReadToEnd().Split(System.Environment.NewLine);
                        foreach (var line in lines)
                        {
                            var sqlStatement = line.Trim();
                            if (sqlStatement != string.Empty)
                            {
                                session.CreateSQLQuery(sqlStatement).ExecuteUpdate();
                            }
                        }

                        transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
#endif

                return sessionFactory;

            });

            services.AddScoped<ISession>((provider) =>
            {
                var factory = provider.GetService<ISessionFactory>();
                return factory.OpenSession();
            });

        }
        private ConventionModelMapper CreateConventionalMappings()
        {
            var mapper = new ConventionModelMapper();

            var baseEntityType = typeof(BaseEntity);
            mapper.IsEntity((t, declared) => baseEntityType.IsAssignableFrom(t) && baseEntityType != t && !t.IsInterface);
            mapper.IsRootEntity((t, declared) => baseEntityType.Equals(t.BaseType));

            mapper.BeforeMapManyToOne += (insp, prop, map) => map.Column(prop.LocalMember.GetPropertyOrFieldType().Name.ToLower() + "id");
            mapper.BeforeMapManyToOne += (insp, prop, map) => map.Cascade(Cascade.Persist);
            mapper.BeforeMapBag += (insp, prop, map) => map.Key(km => km.Column(prop.GetContainerEntity(insp).Name + "id"));
            mapper.BeforeMapBag += (insp, prop, map) => map.Cascade(Cascade.All);
            mapper.BeforeMapClass += (modelInspector, type, map) =>
            {
                map.Table($"{type.Name.ToLower()}");
                map.Id(m =>
                {                  
                    m.Column($"{type.Name.ToLower()}id");
                    m.Generator(Generators.Identity);
                });
            };           

           /* mapper.Class<Zone>(map => map.Component<Position>(x => x.Position, cm =>
            {
                cm.Property(p => p.Latitude, mc => mc.Column("Latitude"));
                cm.Property(p => p.Longitude, mc => mc.Column("Longitude"));
            }));*/

            return mapper;
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            //Middleware provisorio de configuración de autorización.
            app.Use(async (context, next) =>
            {
                if (Configuration.GetValue<bool>("UseTestLogin"))
                {
                    try
                    {
                        var firebaseAuth = new FirebaseAuthProvider(new FirebaseConfig("AIzaSyAhKx_ibS2ENE2Kw01E8mD-KKaXjiQGybU"));
                        var link = await firebaseAuth.SignInWithEmailAndPasswordAsync("jennifercarren@gmail.com", "14103021");

                        context.Request.Headers.Add(TOKEN_HEADER_NAME, link.FirebaseToken);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }

                    await next.Invoke();

                }
            });

            //Middleware de auteticación. Se verifica si el usuraio está autenticado o no.
            app.Use(async (context, next) =>
            {
                var firebaseApp = app.ApplicationServices.GetService<FirebaseApp>();

                var firebaseAuth = FirebaseAdmin.Auth.FirebaseAuth.GetAuth(firebaseApp);

                if (context.Request.Headers.ContainsKey(TOKEN_HEADER_NAME))
                {
                    try
                    {
                        var idToken = context.Request.Headers[TOKEN_HEADER_NAME];
                        var token = await firebaseAuth.VerifyIdTokenAsync(idToken.First());

                        var user = await firebaseAuth.GetUserAsync(token.Uid);
                        context.Items.Add("CurrentUser", user);

                        await next.Invoke();
                    }
                    catch (FirebaseAdmin.Auth.FirebaseAuthException authException)
                    {
                        throw new UnauthorizedException(authException);
                    }
                }
                else
                {
                    throw new UnauthorizedException();
                }
            });


            app.Use(async (context, next) =>
            {
                var serviceScope = app.ApplicationServices.CreateScope();
                var session = serviceScope.ServiceProvider.GetService<ISession>();
                if (session == null) throw new ArgumentNullException(nameof(session));

                try
                {
                    session.BeginTransaction();
                    await next.Invoke();
                    session.Transaction.Commit();
                }
                catch (Exception e)
                {
                    session.Transaction.Rollback();
                }
            });




            app.UseMvc();
        }
    }
}
