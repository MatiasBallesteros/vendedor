﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Servidor.DTOs;
using Servidor.DTOs.Api;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            this._clientService = clientService;
        }

        // GET api/client
        [HttpGet]
        [Route("client")]
        public async Task<ActionResult<IEnumerable<ClientDTO>>> Get()
        {
            return await this._clientService.Get();
        }

        // GET api/client/{id}
        [HttpGet]
        [Route("client/{id}")]
        public async Task<ActionResult<ClientDTO>> GetClient(int id)
        {
            return await this._clientService.GetClient(id);
        }
    }   
}