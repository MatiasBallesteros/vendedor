﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Newtonsoft.Json;
using Servidor.DTOs.Api;
using Servidor.Services.Interfaces;

namespace Servidor.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductService _productService;
        public ProductController(IProductService productService)
        {
            this._productService = productService;
        }

        // GET api/product
        [HttpGet]
        [Route("product")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> Get()
        {
            return await this._productService.Get();
        }

        // GET api/product/{id}
        [HttpGet]
        [Route("product/{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(int id)
        {
            return await this._productService.GetProduct(id);
        }
    }
}