﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.DTOs.Api
{
    public class ApiZoneDTO
    {
        public int id { get; set; }
        public int store_number { get; set; }
        public string corridor { get; set; }
        public string side { get; set; }
        public string cabinet { get; set; }
        public string shelf { get; set; }
        public PositionDTO position { get; set; }

    }
}
