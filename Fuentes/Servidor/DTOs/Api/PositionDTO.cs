﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.DTOs.Api
{
    public class PositionDTO
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
