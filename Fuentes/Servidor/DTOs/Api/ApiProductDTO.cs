﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.DTOs.Api
{
    public class ApiProductDTO
    {
        public string name { get; set; }
        public string brand { get; set; }
        public string description { get; set; }

        public string image { get; set; }

    }
}
