﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.DTOs.Api
{
    public class ApiClientDTO
    {
        public string address { get; set; }
        public string cell_phone { get; set; }

        public string email { get; set; }

        public string fixed_phone { get; set; }

        public int id { get; set; }

        public string legal_id { get; set; }

        public string name { get; set; }

        }
}
