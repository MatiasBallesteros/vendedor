﻿using Servidor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.DTOs.Api
{
    public class ProductDTO
    {
        public string brand { get; set; }
        public string name { get; set; }
        public string QR { get; set; }
        public string description { get; set; }
        public int stock { get; set; }

        public string image { get; set; }
    }
}
