﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.DTOs
{
    public class ClientDTO
    {
        public string identification { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public int latitude { get; set; }
        public int longitude { get; set; }
    }
}
