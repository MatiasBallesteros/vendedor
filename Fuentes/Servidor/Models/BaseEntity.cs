﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.Models
{
    public abstract class BaseEntity
    {
        public virtual long id { get; protected set; }
    }
}
