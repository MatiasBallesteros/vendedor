﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.Models
{
    public class Zone : BaseEntity
    {
        private string name { get; set; }
        private double latitudeP1 { get; set; }
        private double lengthP1 { get; set; }
        private double latitudeP2 { get; set; }
        private double lengthP2 { get; set; }

    }
}
