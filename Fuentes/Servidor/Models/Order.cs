﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.Models
{
    public class Order : BaseEntity
    {
        private int dniClient { get; set; }
        private Seller seller { get; set; }
        private DateTime dateCreation { get; set; }
        private double cost { get; set; }
    }
}
