﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Newtonsoft.Json;
using Servidor.DTOs.Api;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
   
    public class ProductService : IProductService

    {
        private IConfiguration _configuration;
        public ProductService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<ActionResult<IEnumerable<ProductDTO>>> Get()
        {
            HttpClient client = new HttpClient();
            string apiProductDirection = _configuration.GetValue<string>("UrlsApi:Product") + ".json";

            HttpResponseMessage responseApiProduct = await client.GetAsync(apiProductDirection, HttpCompletionOption.ResponseHeadersRead);
            List<ProductDTO> productDtoList = new List<ProductDTO>();
            if (responseApiProduct.IsSuccessStatusCode) //200
            {
                try
                {
                    List<ApiProductDTO> apiProduct = JsonConvert.DeserializeObject<List<ApiProductDTO>>(responseApiProduct.Content.ReadAsStringAsync().Result);
                    foreach (ApiProductDTO api in apiProduct)
                    {
                        ProductDTO productDTO = new ProductDTO
                        {
                            name = api.name,
                            brand = api.brand,
                            description = api.description,
                            image = api.image
                        };
                        productDtoList.Add(productDTO);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught.", e);
                }
            }
            else
            {
                Console.WriteLine("Failed to retrieve contact for reason: {0}", responseApiProduct.ReasonPhrase);

                throw new Exception(string.Format(Message.Error_generic, responseApiProduct.Content));
            }

            return productDtoList;
        }

        public async Task<ActionResult<ProductDTO>> GetProduct(int id)
        {
            HttpClient client = new HttpClient();
            string apiProductDirection = _configuration.GetValue<string>("UrlsApi:Product") + "/" + id + ".json";
            HttpResponseMessage responseApiClient = await client.GetAsync(apiProductDirection, HttpCompletionOption.ResponseHeadersRead);
            ProductDTO productDTO = new ProductDTO();
            if (responseApiClient.IsSuccessStatusCode) //200
            {
                try
                {
                    ApiProductDTO apiProduct = JsonConvert.DeserializeObject<ApiProductDTO>(responseApiClient.Content.ReadAsStringAsync().Result);
                    productDTO.name = apiProduct.name;
                    productDTO.brand = apiProduct.brand;
                    productDTO.description = apiProduct.description;
                    productDTO.image = apiProduct.image;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught.", e);
                }
            }
            else
            {
                Console.WriteLine("Failed to retrieve contact for reason: {0}", responseApiClient.ReasonPhrase);
                throw new Exception(string.Format("Failed to retrieve contact for reason: {0}", responseApiClient.Content));
            }

            return productDTO;
        }
    }
}