﻿using Microsoft.AspNetCore.Mvc;
using Servidor.DTOs.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servidor.Services.Interfaces
{
    public interface IProductService
    {
        Task<ActionResult<IEnumerable<ProductDTO>>> Get();

        Task<ActionResult<ProductDTO>> GetProduct(int id);
    }
}
