﻿using Microsoft.AspNetCore.Mvc;
using Servidor.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Servidor.Services.Interfaces
{
    public interface IClientService
    {
        Task<ActionResult<IEnumerable<ClientDTO>>> Get();

        Task<ActionResult<ClientDTO>> GetClient(int id);

        void GenerateZoneClient();
    }
}
