﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Servidor.DTOs;
using Servidor.DTOs.Api;
using Servidor.Services.Interfaces;

namespace Servidor.Services
{
    public class ClientService : IClientService
    {
        private IConfiguration _configuration;

        public ClientService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void GenerateZoneClient()
        {
            throw new NotImplementedException();
        }

        public async Task<ActionResult<IEnumerable<ClientDTO>>> Get()
        {
            HttpClient client = new HttpClient();
            string apiClientDirection = _configuration.GetValue<string>("UrlsApi:Client") + ".json";

            HttpResponseMessage responseApiClient = await client.GetAsync(apiClientDirection, HttpCompletionOption.ResponseHeadersRead);
            List<ClientDTO> clientDtoList = new List<ClientDTO>();
            if (responseApiClient.IsSuccessStatusCode) //200
            {
                try
                {
                    List<ApiClientDTO> apiClient = JsonConvert.DeserializeObject<List<ApiClientDTO>>(responseApiClient.Content.ReadAsStringAsync().Result);
                    foreach (ApiClientDTO api in apiClient)
                    {
                        ClientDTO clientDTO = new ClientDTO
                        {
                            name = api.name,
                            identification = api.legal_id,
                            address = api.address
                        };
                        clientDtoList.Add(clientDTO);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught.", e);
                }
            }
            else
            {
                Console.WriteLine("Failed to retrieve contact for reason: {0}", responseApiClient.ReasonPhrase);
                throw new Exception(string.Format("Failed to retrieve contact for reason: {0}", responseApiClient.Content));
            }

            return clientDtoList;
        }
        
        public async Task<ActionResult<ClientDTO>> GetClient(int id)
        {
            HttpClient client = new HttpClient();
            string apiClientDirection = _configuration.GetValue<string>("UrlsApi:Client") + "/" + id + ".json";
            HttpResponseMessage responseApiClient = await client.GetAsync(apiClientDirection, HttpCompletionOption.ResponseHeadersRead);
            ClientDTO clientDTO = new ClientDTO();
            if (responseApiClient.IsSuccessStatusCode) //200
            {
                try
                {
                    ApiClientDTO apiClient = JsonConvert.DeserializeObject<ApiClientDTO>(responseApiClient.Content.ReadAsStringAsync().Result);
                    clientDTO.name = apiClient.name;
                    clientDTO.identification = apiClient.legal_id;
                    clientDTO.address = apiClient.address;
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught.", e);
                }
            }
            else
            {
                Console.WriteLine("Failed to retrieve contact for reason: {0}", responseApiClient.ReasonPhrase);
                throw new Exception(string.Format("Failed to retrieve contact for reason: {0}", responseApiClient.Content));
            }

            return clientDTO;
        }
    }
}